import time
import subprocess
import os
import threading
import Queue

from kivy._event import EventDispatcher
from kivy.app import App
from kivy.core.window import Window
from kivy.uix.textinput import TextInput
from kivy.uix.vkeyboard import VKeyboard
from kivy.utils import platform
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen, ScreenManager, FadeTransition
from kivy.properties import NumericProperty

from printrun import gcoder
import serial


screen_manager = ScreenManager(transition=FadeTransition())


class ClaimingScreen(Screen):
    pass


class PaymentScreen(Screen):
    price = 0
    model_file = ''

    def setFile(self, file):
        self.model_file = file

    def setPrice(self, p):
        self.price = p

    def on_enter(self, *args):
        self.price_pay.text = 'Amount Due: ' + str(self.price)
        time.sleep(3)
        screen_manager.current =  'printscreen'
        screen_manager.current_screen.model_file_dir = self.model_file


class PrintScreen(Screen):
    model_file_dir = ''
    f = None
    s = None

    def on_enter(self, *args):
        time.sleep(2)
        self.create_print_thread()

    def create_print_thread(self):
        screen_manager.current = 'homescreen'
        print_thread = threading.Thread(target=self.start_print)
        print_thread.start()

    def start_print(self):
        head, tail = os.path.split(self.model_file_dir)
        print self.model_file_dir
        if platform == 'win':
            self.connectPort('COM5')
            self.openFile('out_gcode/' + os.path.splitext(tail)[0] + '.gcode')
            self.wakeUp()
            self.send_gcode()
        elif platform == 'linux':
            self.connectPort('/dev/ttyUSB0')
            self.openFile('out_gcode/' + os.path.splitext(tail)[0] + '.gcode')
            self.wakeUp()
            self.send_gcode()
            pass

    def connectPort(self, port):
        try:
            self.s = serial.Serial(port, 115200)
        except:
            print 'error port connection'

    def removeComment(self, string):
        if (string.find(';') == -1):
            return string
        else:
            return string[:string.index(';')]

    def openFile(self, file):
        try:
            self.f = open(file, 'r')
        except:
            print 'error in reading file'

    def wakeUp(self):
        self.s.write("\r\n\r\n")  # Hit enter a few times to wake the Printrbot
        time.sleep(2)  # Wait for Printrbot to initialize
        self.s.flushInput()  # Flush startup text in serial input
        print 'Sending gcode'

    def send_gcode(self):
        self.wakeUp()

        for line in self.f:
            l = self.removeComment(line)
            l = l.strip()  # Strip all EOL characters for streaming
            if (l.isspace() == False and len(l) > 0):
                print 'Sending: ' + l
                self.s.write(l + '\n')  # Send g-code block
                grbl_out = self.s.readline()  # Wait for response with carriage return
                print ' : ' + grbl_out.strip()

        self.s.close()
        self.f.close()


class AssessmentScreen(Screen):
    model_file_dir = ''
    layer_count = NumericProperty(0)

    def on_enter(self, *args):
        filament_price = 900.00 #1kg
        filament_length = 300000 #per 1kg
        unit_price = filament_price/filament_length #price per mm

        head, tail = os.path.split(self.model_file_dir)
        gcode = gcoder.GCode(open('out_gcode/' + os.path.splitext(tail)[0] + '.gcode'))

        self.layer_count = gcode.layers_count

        if gcode.duration.total_seconds() < 3600:
            eta = self.strfdelta(gcode.duration, "{minutes}mins")
        elif (gcode.duration.total_seconds() % 3600) == 0:
            eta = self.strfdelta(gcode.duration, "{hours}hrs")
        else:
            eta = self.strfdelta(gcode.duration, "{hours}hrs {minutes}mins")

        info = 'Length of Material: ' + str(gcode.filament_length) + 'mm\nEstimated Print Time: ' + eta + '\nBox Dimensions: ' + str(gcode.depth) + ' x ' + str(gcode.width) + ' x ' + str(gcode.height) + ' mm'
        self.assessment_label.text = info

        total_price = (gcode.filament_length * unit_price) + 200
        self.price = round(total_price,-1)
        self.price_label.text = 'P' + str("%.2f" % self.price)

    def getPrice(self):
        return self.price

    def update_file_dir(self, file_dir):
        self.model_file_dir = file_dir
        # print self.model_file_dir

    def strfdelta(self, tdelta, fmt):
        d = {"days": tdelta.days}
        d["hours"], rem = divmod(tdelta.seconds, 3600)
        d["minutes"], d["seconds"] = divmod(rem, 60)
        return fmt.format(**d)

class SlicingScreen(Screen):
    model_file_dir = ''
    load_index = 0

    def update_file_dir(self, file_dir):
        self.model_file_dir = file_dir

    def on_enter(self, *args):
        slice_thread = threading.Thread(target=self.slice_model)
        slice_thread.start()

        while slice_thread.isAlive() is True:
            pass

        self.slicing_done()

    def slicing_done(self):
        screen_manager.current = 'assessmentscreen'
        screen_manager.current_screen.update_file_dir(self.model_file_dir)

    def slice_model(self):
        file_dir = self.model_file_dir
        head, tail = os.path.split(file_dir)

        if platform == 'win':
            command = "Slic3r-win/slic3r-console.exe " + file_dir + " --load config.ini --output out_gcode/" + os.path.splitext(tail)[0] + ".gcode"
            if subprocess.call(command, shell=False) == 0:
                return
            else:
                print 'Error in Slicing'
        elif platform == 'linux':
            command = "Slic3r-linux/bin/slic3r " + file_dir + " --load config.ini --output out_gcode/" + os.path.splitext(tail)[0] + ".gcode"
            if subprocess.call(command, shell=False) == 0:
                return
            else:
                print 'Error in Slicing'

class HomeScreen(Screen):
    def __init__(self, **kw):
        super(HomeScreen, self).__init__(**kw)
        self.show_time()
        self.update_time()

    def show_time(self, *args):
        self.time_label.text = time.strftime("%a, %b %d | %I:%M %p")

    def update_time(self):
        Clock.schedule_interval(self.show_time, 1)


class FileChooserScreen(Screen):
    rootpath = None

    def select(self, *args):
        try:
            self.file_name_label.text = args[1][0]
        except:
            pass

    def getRootPath(self):
        if platform == 'win':
            return 'C:/users/rupert/desktop'
        elif platform == 'linux':
            return '/media/pi/'


class AxyzApp(App):

    def build(self):
        self.setupscreens()
        return screen_manager

    def setupscreens(self):
        home_screen = HomeScreen(name="homescreen")
        file_chooser_screen = FileChooserScreen(name="filechooserscreen")
        slicing_screen = SlicingScreen(name='slicingscreen')
        assessment_screen = AssessmentScreen(name='assessmentscreen')
        payment_screen = PaymentScreen(name='paymentscreen')
        print_screen = PrintScreen(name='printscreen')
        claiming_screen = ClaimingScreen(name='claimingscreen')

        screen_manager.add_widget(home_screen)
        screen_manager.add_widget(print_screen)
        screen_manager.add_widget(claiming_screen)
        screen_manager.add_widget(payment_screen)
        screen_manager.add_widget(file_chooser_screen)
        screen_manager.add_widget(slicing_screen)
        screen_manager.add_widget(assessment_screen)


if __name__ == '__main__':
    AxyzApp().run()
